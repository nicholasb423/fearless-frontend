function createCard(name, description, pictureUrl, formatStart, formatEnd, location) {
    return `
      <div class="card shadow border-light mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h4 class="card-title">${name}</h4>
          <h6 class="card-subtitle text-muted mb-2">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${formatStart} - ${formatEnd}</div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error('Failed to fetch conferences. Status: ' + response.status);
      } else {
        const data = await response.json();

        for (let [index, conference] of data.conferences.entries()) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date((details.conference.starts));
            const formatStart = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`
            const endDate = new Date((details.conference.ends));
            const formatEnd = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, formatStart, formatEnd, location);
            const column = document.querySelector(`.column-${index % 3 + 1}`);
            column.innerHTML += html;
            console.log(formatStart, formatEnd)
          }
        }

      }
    } catch (e) {
        console.error(e);
        const errorAlert = document.getElementById('errorAlert');
        errorAlert.textContent = e.message;
        errorAlert.classList.remove('d-none');
    }

  });
